package websiteinfo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Teavitaja {

	private int newestThread = -1;
	private String emailText;
	private String checkUrl;

	public Teavitaja(String url, String emailText) {
		checkUrl = url;
		this.emailText = emailText;
		newestThread = getNewestThreadNumber();
	}

	public Integer extractNumber(String line) {
		/**
		 * vaatab kas rida sisaldab teatud kombinatsiooni, et leida kuulutuste
		 * numbrid
		 */
		if (!line.contains("class=\" item_row\">"))
			return null;

		/**
		 * eemaldab ebavajalikud tähised, et kätte saada ainult kuulutuse number
		 */
		int nodeIndex = line.indexOf("\"item_") + 6;
		String toEnd = line.substring(nodeIndex, line.length() - 1);

		int quoteIndex = toEnd.indexOf("\"");
		String numStr = toEnd.substring(0, quoteIndex);
		Integer num = Integer.parseInt(numStr);

		return num;
	}

	public int getNewestThreadNumber() {
		/**
		 * veebileht millelt kuulutused võetakse
		 */
		URL url;
		try {
			url = new URL(checkUrl);

			URLConnection con = url.openConnection();
			InputStream is = con.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			String line = null;

			Integer maxNumber = 0;

			/**
			 * loeb igat rida ja prindib välja kuulutuse suurima numbri
			 */
			while ((line = br.readLine()) != null) {
				Integer num = extractNumber(line);
				if (num != null && num > maxNumber) {
					maxNumber = num;
				}
			}
			return maxNumber;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * source code
	 * http://stackoverflow.com/questions/5371943/reading-from-a-url-connection-
	 * java
	 */

	public void check() {
		int threadNum = getNewestThreadNumber();
		if (threadNum > newestThread) {
			newestThread = threadNum;
			SendEmail();
		}
	}

	public void SendEmail() {
		/**
		 * email kellele saadetakse
		 */
		String to = "lisandra123@hotmail.com";

		/**
		 * email kellelt saadetakse
		 */
		String from = "poletestiadress@gmail.com";
		String password = "SeePoleParool";
		Integer port = 465;

		/**
		 * meili serveri aadress
		 */
		String host = "smtp.gmail.com";

		/**
		 * süsteemi property
		 */
		Properties properties = System.getProperties();

		/**
		 * property nimi mille kaudu ta otsib hosti
		 */
		properties.setProperty("mail.smtp.host", host);

		/**
		 * Sessiooni objekt
		 */
		Session session = Session.getDefaultInstance(properties);

		try {

			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(from));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			/**
			 * emaili pealkiri
			 */
			message.setSubject("Uus kuulutus");

			/**
			 * emaili sisu
			 */
			message.setText(emailText);

			/**
			 * emaili saatmine
			 */
			Transport t = session.getTransport("smtps");
			t.connect(host, port, from, password);
			t.sendMessage(message, message.getAllRecipients());
			t.close();

			/**
			 * teatamine, et email on saadetud
			 */
			System.out.println("Sent message successfully...");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}
	/**
	 * source code http://www.tutorialspoint.com/java/java_sending_email.htm
	 */
}