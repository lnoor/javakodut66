package websiteinfo;

public class MesindusTeavitaja extends Teavitaja {
	public MesindusTeavitaja(String url, String emailText) {
		super(url, emailText);
	}

	/**
	 * Mesindus.ee puhul kasuta teistsugust meetodit numbri leidmiseks.
	 */
	@Override
	public Integer extractNumber(String line) {
		/**
		 * vaatab kas rida sisaldab teatud kombinatsiooni,et saada kuulutuse
		 * number
		 */
		if (!line.contains("<td class=\"title\"><a href=\"/node/"))
			return null;

		/**
		 * eemaldab ebavajalikud tähised, et kätte saada ainult kuulutuse number
		 */
		int nodeIndex = line.indexOf("/node/") + 6;
		String toEnd = line.substring(nodeIndex, line.length() - 1);

		int quoteIndex = toEnd.indexOf("\"");
		String numStr = toEnd.substring(0, quoteIndex);
		Integer num = Integer.parseInt(numStr);

		return num;
	}
}