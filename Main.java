package websiteinfo;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws IOException {
		ArrayList<Teavitaja> teavitajad = new ArrayList<>();
		/**
		 * V�tab teatud lehe kuulutuse nr ja saadab erineva emaili erineva veebilehe puhul
		 */
		teavitajad.add(new Teavitaja("http://www.soov.ee/eesti?q=&cg=1020&w=3&ca=1&l=0&md=th",
				"Uus auto kuulutus on postitatud!"));
		teavitajad.add(new Teavitaja("http://www.soov.ee/eesti/arvutid?ca=1&w=3&cg=5020&st=s&st=u&st=b&st=k&st=h",
				"Uus arvuti kuulutus on postitatud!"));
		teavitajad.add(new MesindusTeavitaja("http://mesindus.ee/foorumid/kuulutused",
				"Uus mesindus kuulutus on postitatud!"));

		Integer minutesToWait = 10;

		while (true) {
			for (Teavitaja teavitaja : teavitajad) {
				teavitaja.check();
			}
			/**
			 * Kontrollib lehtede kuulutuste numbreid iga 10 minuti tagant
			 */
			try {
				Thread.sleep(minutesToWait * 60 * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}